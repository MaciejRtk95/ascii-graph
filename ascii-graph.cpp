#include <iostream>

std::string asciiGraph(int numberOfPoints, int points[][2], int filling = 1) {
    //The graph has a 1 char margin of empty space with the border definers being the points and/or axis crosspoint
    int minX = -1, maxX = 1, minY = -1, maxY = 1;

    //First we find the size of the graph by means of where does each axis start and end
    for (int i = 0; i < numberOfPoints; i++) {
        if (points[i][0] <= minX)
            minX = points[i][0] - 1;
        else if (points[i][0] >= maxX)
            maxX = points[i][0] + 1;

        if (points[i][1] <= minY)
            minY = points[i][1] - 1;
        else if (points[i][1] >= maxY)
            maxY = points[i][1] + 1;
    }

    //Then we actually make the graph
    std::string graph;
    for (int y = maxY; y >= minY; y--) {
        for (int x = minX; x <= maxX; x++) {
            bool onAPoint = false;
            for (int i = 0; i < numberOfPoints; i++)
                if (points[i][0] == x && points[i][1] == y)
                    onAPoint = true;

            if (onAPoint)
                graph += "*";
            else if (x == 0 && y == 0)
                graph += "+";
            else if (x == 0)
                graph += "|";
            else if (y == 0)
                graph += "-";
            else if (filling == 2)
                graph += ".";
            else if (filling == 1) {
                bool validForFilling = false;
                for (int i = 0; i < numberOfPoints; i++) {
                    if (points[i][0] == x) {
                        if (y > 0 && points[i][1] > y) {
                            graph += '.';
                            validForFilling = true;
                            break;
                        } else if (y < 0 && points[i][1] < y) {
                            graph += '.';
                            validForFilling = true;
                            break;
                        }
                    } else if (points[i][1] == y) {
                        if (x > 0 && points[i][0] > x) {
                            graph += '.';
                            validForFilling = true;
                            break;
                        } else if (x < 0 && points[i][0] < x) {
                            graph += '.';
                            validForFilling = true;
                            break;
                        }
                    }
                }
                if (!validForFilling)
                    graph += ' ';
            } else
                graph += ' ';
        }
        graph += "\n";
    }

    return graph;
}
