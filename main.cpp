#include "ascii-graph.cpp"

bool isANumber(std::string subject) {
    for (int i = 0; i < subject.length(); i++)
        if (!(subject[i] >= '0' && subject[i] <= '9') && subject[i] != '-')
            return false;
    return true;
}

void printUsage(std::string callCommand) {
    std::cout << "Usage: " << callCommand << " numberOfPoints XOfPoint1 YOfPoint1 XOfPoint2 YOfPoint2 ...\n"
              << '\n'
              << "All arguments must be integers. Negative integers are also acceptable." << std::endl;
}

int main(int argc, char* argv[]) {
    //Argument error-checking
    for (int i = 1; i < argc; i++)
        if (!isANumber(argv[i])) {
            printUsage(argv[0]);
            return 0;
        }
    if (argc < 2) {
        printUsage(argv[0]);
        std::cout << "Pass me some arguments!" << std::endl;
        return 0;
    } else if (argc < (atoi(argv[1]) * 2) + 2) {
        printUsage(argv[0]);
        std::cout << "You missed something (you gave less arguments than you said you would give)." << std::endl;
        return 0;
    } else if (argc > (atoi(argv[1]) * 2) + 2) {
        printUsage(argv[0]);
        std::cout << "You gave more arguments than you said you would." << std::endl;
        return 0;
    }


    //Actual program
    int numberOfPoints;
    numberOfPoints = atoi(argv[1]);
    int points[numberOfPoints][2];
    for (int i = 0, j = 2; i < numberOfPoints; i++) {
        points[i][0] = atoi(argv[j]);
        j++;
        points[i][1] = atoi(argv[j]);
        j++;
    }

    std::cout << '\n' << asciiGraph(numberOfPoints, points);

    return 0;
}
